package com.obi.awesomeapp.ui.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import com.obi.awesomeapp.databinding.ActivityMainBinding
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.snackbar.Snackbar
import com.obi.awesomeapp.R
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem

import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class HomeActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var homeViewModel: HomeViewModel
    private var menu: Menu? = null

    private var isLoading: Boolean = false
    private var currentPerPage = 15

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.collapsingToolbar.title = getString(R.string.app_name)
        binding.collapsingToolbar.setCollapsedTitleTextColor(
            ContextCompat.getColor(this, R.color.orange_700)
        )

        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(false)

        //Show title when collapsed
        var isShow = true
        var scrollRange = -1
        binding.appbar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { barLayout, verticalOffset ->
            if (scrollRange == -1){
                scrollRange = barLayout?.totalScrollRange!!
            }
            if (scrollRange + verticalOffset == 0){
                binding.collapsingToolbar.title = "Awesome App"
                isShow = true
            } else if (isShow){
                binding.collapsingToolbar.title = " "
                isShow = false
            }
        })

        setupViewModel()
        setupRecycler()
        addOnScroll()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_list, menu)
        this.menu = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val menuItemGrid = menu?.findItem(R.id.action_grid)
        val menuItemList = menu?.findItem(R.id.action_list)
        return when (item.itemId) {
            R.id.action_grid -> {
                binding.rvPhotos.apply {
                    layoutManager = GridLayoutManager(context, 2)
                }
                menuItemGrid?.icon = ContextCompat.getDrawable(this, R.drawable.ic_baseline_grid_on_24)
                menuItemList?.icon = ContextCompat.getDrawable(this, R.drawable.ic_baseline_view_list_24)
                true
            }
            R.id.action_list -> {
                binding.rvPhotos.apply {
                    layoutManager = LinearLayoutManager(context)
                }
                menuItemGrid?.icon = ContextCompat.getDrawable(this, R.drawable.ic_baseline_grid_24)
                menuItemList?.icon = ContextCompat.getDrawable(this, R.drawable.ic_baseline_view_list_on_24)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupViewModel() {
        homeViewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
        homeViewModel.listPhotos.observe(this, {
            binding.rvPhotos.adapter?.let { adapter ->
                if (adapter is HomeAdapter) {
                    adapter.setListArea(it!!)
                }
            }
        })

        fetch()

        homeViewModel.isLoading.observe(this, {
            binding.progressBar.visibility = if (it) View.VISIBLE else View.GONE
        })
        homeViewModel.isLoadingMore.observe(this@HomeActivity, {
            binding.progressBarMore.visibility = if (it) View.VISIBLE else View.GONE
        })

        homeViewModel.isMessage.observe(this, {
            val snack: Snackbar = Snackbar.make(binding.coordinator, it, Snackbar.LENGTH_LONG)
            val view = snack.view
            val params = view.layoutParams as CoordinatorLayout.LayoutParams
            params.gravity = Gravity.TOP
            view.layoutParams = params
            snack.show()
            if(it.isNotEmpty()){
                snack.view.setBackgroundColor(ContextCompat.getColor(this, R.color.red))
            }
        })
        homeViewModel.isEmpty.observe(this, {
            binding.tvEmptyStored.visibility = if (it) View.VISIBLE else View.GONE
        })
    }

    private fun fetch() {
        isLoading = false
        homeViewModel.getAllPhotos(1, currentPerPage)
    }

    private fun setupRecycler() {
        binding.rvPhotos.apply {
            layoutManager = GridLayoutManager(context, 2)
            adapter = HomeAdapter(mutableListOf(), context)
        }
    }

    private fun addOnScroll() {
        //attaches scrollListener with RecyclerView
        binding.rvPhotos.addOnScrollListener(object : RecyclerView.OnScrollListener()
        {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int)
            {
                super.onScrolled(recyclerView, dx, dy)
                val linearLayoutManager = recyclerView.layoutManager as LinearLayoutManager?
                if (!isLoading)
                {
                    //findLastCompletelyVisibleItemPostition() returns position of last fully visible view.
                    ////It checks, fully visible view is the last one.
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == currentPerPage - 1)
                    {
                        currentPerPage += 15
                        isLoading = true
                        binding.progressBarMore.visibility = View.VISIBLE
                        fetch()
                    }
                }
            }
        })
    }
}
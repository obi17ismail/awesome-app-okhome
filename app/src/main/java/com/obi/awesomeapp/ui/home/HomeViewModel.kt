package com.obi.awesomeapp.ui.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.obi.awesomeapp.api.ApiConfig
import com.obi.awesomeapp.data.CuratedResponse
import com.obi.awesomeapp.data.PhotosItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeViewModel : ViewModel() {

    private val _listPhotos = MutableLiveData<ArrayList<PhotosItem>>()
    val listPhotos: LiveData<ArrayList<PhotosItem>> = _listPhotos

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _isLoadingMore = MutableLiveData<Boolean>()
    val isLoadingMore: LiveData<Boolean> = _isLoadingMore

    private val _isMessage = MutableLiveData<String>()
    val isMessage: LiveData<String> = _isMessage

    private val _isEmpty = MutableLiveData<Boolean>()
    val isEmpty: LiveData<Boolean> = _isEmpty

    companion object{
        private const val TAG = "HomeViewModel"
        private const val AUTHORIZATION = "563492ad6f9170000100000126b9e2526ccf435c94f51c2dd86726d7"
    }

    fun getAllPhotos(page: Int, perPage: Int) {
        _isLoading.value = true
        val client = ApiConfig.getApiService().getCurated(AUTHORIZATION, page, perPage)
        client.enqueue(object : Callback<CuratedResponse> {
            override fun onResponse(
                call: Call<CuratedResponse>,
                response: Response<CuratedResponse>
            ) {
                _isLoading.value = false
                _isLoadingMore.value = false
                if (response.isSuccessful) {
                    if(response.body()?.photos!!.size > 0){
                        _isEmpty.value = false
                        _listPhotos.value = response.body()?.photos
                    } else {
                        _isEmpty.value = true
                    }
                    Log.e(TAG, "onSuccess:")
                } else {
                    _isMessage.value = "Terjadi kesalahan. Gagal mendapatkan response"
                    Log.e(TAG, "onFailure: ${response.message()}")
                }
            }

            override fun onFailure(call: Call<CuratedResponse>, t: Throwable) {
                _isLoading.value = false
                _isLoadingMore.value = false
                _isMessage.value = "Tidak Ada Koneksi Internet"
                Log.e(TAG, "onFailure2: ${t.message.toString()}")
            }
        })
    }
}
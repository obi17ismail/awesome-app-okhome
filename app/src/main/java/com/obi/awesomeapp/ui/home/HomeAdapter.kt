package com.obi.awesomeapp.ui.home

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.obi.awesomeapp.data.PhotosItem
import com.obi.awesomeapp.databinding.ItemPhotoBinding
import com.obi.awesomeapp.ui.detail.DetailActivity

class HomeAdapter(private var listArea: MutableList<PhotosItem>, private var context: Context) :
    RecyclerView.Adapter<HomeAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemPhotoBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ViewHolder(binding)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setListArea(r: ArrayList<PhotosItem>) {
        listArea.clear()
        listArea.addAll(r)
        notifyDataSetChanged()
    }

    override fun getItemCount() = listArea.size
    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(listArea[position], context)

    class ViewHolder(val binding: ItemPhotoBinding) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("SetTextI18n")
        fun bind(photo: PhotosItem, context: Context) {
            binding.ivPicture.load(photo.src.medium)
            binding.tvItemDescription.text = photo.photographer
            itemView.setOnClickListener {
                context.startActivity(Intent(context, DetailActivity::class.java).apply {
                    putExtra("id", photo.id)
                })
            }
        }
    }

}
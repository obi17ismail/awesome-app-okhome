package com.obi.awesomeapp.ui.detail

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.Gravity
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.google.android.material.snackbar.Snackbar
import com.obi.awesomeapp.R
import androidx.lifecycle.Observer
import coil.load
import com.obi.awesomeapp.databinding.ActivityDetailBinding

class DetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailBinding
    private lateinit var detailViewModel: DetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.toolbar.setTitleTextColor(Color.WHITE)
        binding.toolbar.setBackgroundColor(resources.getColor(R.color.black_transparant))
        binding.collapsingToolbar.setCollapsedTitleTextColor(
            ContextCompat.getColor(this, R.color.orange_700)
        )
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.toolbar.setNavigationIcon(R.drawable.ic_baseline_arrow_back_white_24)
        binding.toolbar.setNavigationOnClickListener {
            onBackPressed()
        }

        setupViewModel()
    }

    @SuppressLint("SetTextI18n")
    private fun setupViewModel() {
        detailViewModel = ViewModelProvider(this).get(DetailViewModel::class.java)
        detailViewModel.photos.observe(this, Observer {
            binding.toolbar.title = "Detail ${it.photographer}"
            binding.ivPhoto.load(it.src.large)
            binding.tvPhotographerDetail.text = it.photographer

            binding.tvUrlDetail.isClickable = true
            binding.tvUrlDetail.movementMethod = LinkMovementMethod.getInstance()
            binding.tvUrlDetail.text = Html.fromHtml(it.url)

            binding.tvColorDetail.text = it.avgColor
            binding.tvLikedDetail.text = it.liked.toString()

            binding.btnShareDetail.setOnClickListener { _ ->
                val shareIntent= Intent()
                shareIntent.action= Intent.ACTION_SEND
                shareIntent.putExtra(Intent.EXTRA_TEXT, it.photographer+" - "+it.url)
                shareIntent.type="text/plain"
                startActivity(Intent.createChooser(shareIntent,"Bagikan Ke:"))
            }
        })

        fetch()

        detailViewModel.isLoading.observe(this, {
            binding.progressBar.visibility = if (it) View.VISIBLE else View.GONE
        })

        detailViewModel.isMessage.observe(this, {
            val snack: Snackbar = Snackbar.make(binding.coordinator, it, Snackbar.LENGTH_LONG)
            val view = snack.view
            val params = view.layoutParams as CoordinatorLayout.LayoutParams
            params.gravity = Gravity.TOP
            view.layoutParams = params
            snack.show()
            if(it.isNotEmpty()){
                snack.view.setBackgroundColor(ContextCompat.getColor(this, R.color.red))
            }
        })
    }

    private fun fetch() {
        detailViewModel.getDetailPhoto(intent.getIntExtra("id", 0))
    }
}
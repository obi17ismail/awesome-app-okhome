package com.obi.awesomeapp.ui.detail

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.obi.awesomeapp.api.ApiConfig
import com.obi.awesomeapp.data.PhotosItem
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DetailViewModel : ViewModel() {

    private val _photos = MutableLiveData<PhotosItem>()
    val photos: LiveData<PhotosItem> = _photos

    private val _isLoading = MutableLiveData<Boolean>()
    val isLoading: LiveData<Boolean> = _isLoading

    private val _isMessage = MutableLiveData<String>()
    val isMessage: LiveData<String> = _isMessage

    companion object{
        private const val TAG = "HomeViewModel"
        private const val AUTHORIZATION = "563492ad6f9170000100000126b9e2526ccf435c94f51c2dd86726d7"
    }

    fun getDetailPhoto(id: Int) {
        _isLoading.value = true
        val client = ApiConfig.getApiService().getDetailPhoto(AUTHORIZATION, id)
        client.enqueue(object : Callback<PhotosItem> {
            override fun onResponse(
                call: Call<PhotosItem>,
                response: Response<PhotosItem>
            ) {
                _isLoading.value = false
                if (response.isSuccessful) {
                    _photos.value = response.body()
                    Log.e(TAG, "onSuccess:")
                } else {
                    _isMessage.value = "Terjadi kesalahan. Gagal mendapatkan response"
                    Log.e(TAG, "onFailure: ${response.message()}")
                }
            }

            override fun onFailure(call: Call<PhotosItem>, t: Throwable) {
                _isLoading.value = false
                _isMessage.value = "Tidak Ada Koneksi Internet"
                Log.e(TAG, "onFailure2: ${t.message.toString()}")
            }
        })
    }
}
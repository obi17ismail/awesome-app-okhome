package com.obi.awesomeapp.api

import com.obi.awesomeapp.data.CuratedResponse
import com.obi.awesomeapp.data.PhotosItem
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    @GET("curated")
    fun getCurated(
        @Header("Authorization") token: String,
        @Query("page") page: Int,
        @Query("per_page") perPage: Int
    ): Call<CuratedResponse>

    @GET("photos/{id_photo}")
    fun getDetailPhoto(
        @Header("Authorization") token: String,
        @Path("id_photo") idPhoto: Int,
    ): Call<PhotosItem>
}
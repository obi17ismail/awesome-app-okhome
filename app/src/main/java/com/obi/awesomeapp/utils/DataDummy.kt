package com.obi.awesomeapp.utils

import com.obi.awesomeapp.data.PhotosItem
import com.obi.awesomeapp.data.Src

import java.util.ArrayList

object DataDummy {

    fun generateDummyPhotos(): ArrayList<PhotosItem> {

        val photos = ArrayList<PhotosItem>()

        photos.add(
            PhotosItem(
                generateDummySrc()[0],
                2592,
                "#343743",
                "Simon Gough",
                "https://www.pexels.com/@scgough",
                1,
                "https://www.pexels.com/photo/silhouette-of-mountains-7365270/",
                1785348,
                false,
                1680)
        )
        photos.add(
            PhotosItem(
                generateDummySrc()[1],
                2397,
                "#999A94",
                "Yaroslava Borz",
                "https://www.pexels.com/@yaroslava-borz-126286496",
                2,
                "https://www.pexels.com/photo/woman-in-black-tank-top-and-brown-fedora-hat-standing-on-green-grass-field-10012826/",
                126286496,
                false,
                3578)
        )
        photos.add(
            PhotosItem(
                generateDummySrc()[2],
                2397,
                "#626D6F",
                "Simon Gough",
                "https://www.pexels.com/@scgough",
                3,
                "https://www.pexels.com/photo/stainless-steel-framed-brown-padded-chair-10013212/",
                126286496,
                false,
                3578)
        )
        photos.add(
            PhotosItem(
                generateDummySrc()[3],
                2000,
                "#838383",
                "Yaroslava Borz",
                "https://www.pexels.com/@yaroslava-borz-126286496",
                4,
                "https://www.pexels.com/photo/grayscale-photo-of-woman-in-black-floral-shirt-10012544/",
                126286496,
                false,
                2605)
        )
        photos.add(
            PhotosItem(
                generateDummySrc()[4],
                3464,
                "#90968A",
                "Yaroslava Borz",
                "https://www.pexels.com/@yaroslava-borz-126286496",
                5,
                "https://www.pexels.com/photo/woman-in-red-jacket-walking-on-sidewalk-10012571/",
                126286496,
                false,
                4618)
        )
        photos.add(
            PhotosItem(
                generateDummySrc()[5],
                3464,
                "#7A837E",
                "Yaroslava Borz",
                "https://www.pexels.com/@yaroslava-borz-126286496",
                6,
                "https://www.pexels.com/photo/clear-wine-glass-on-white-table-10012572/",
                126286496,
                false,
                4618)
        )
        photos.add(
            PhotosItem(
                generateDummySrc()[6],
                3464,
                "#7C837C",
                "Yaroslava Borz",
                "https://www.pexels.com/@yaroslava-borz-126286496",
                7,
                "https://www.pexels.com/photo/black-bicycle-parked-beside-glass-window-10012573/",
                126286496,
                false,
                4618)
        )
        photos.add(
            PhotosItem(
                generateDummySrc()[7],
                2000,
                "#615A52",
                "Виктория Ривьер",
                "https://www.pexels.com/@126286771",
                8,
                "https://www.pexels.com/photo/fashion-people-woman-summer-10012402/",
                126286771,
                false,
                2500)
        )

        return photos
    }

    fun generateDummyPhotosEmpty(): ArrayList<PhotosItem> {

        return ArrayList()
    }

    private fun generateDummySrc(): ArrayList<Src> {

        val srcs = ArrayList<Src>()

        srcs.add(
            Src(
                "https://images.pexels.com/photos/7365270/pexels-photo-7365270.jpeg?auto=compress&cs=tinysrgb&h=130",
                "https://images.pexels.com/photos/7365270/pexels-photo-7365270.jpeg",
                "https://images.pexels.com/photos/7365270/pexels-photo-7365270.jpeg?auto=compress&cs=tinysrgb&h=650&w=940",
                "https://images.pexels.com/photos/7365270/pexels-photo-7365270.jpeg?auto=compress&cs=tinysrgb&dpr=1&fit=crop&h=200&w=280",
                "https://images.pexels.com/photos/7365270/pexels-photo-7365270.jpeg?auto=compress&cs=tinysrgb&h=350",
                "https://images.pexels.com/photos/7365270/pexels-photo-7365270.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
                "https://images.pexels.com/photos/7365270/pexels-photo-7365270.jpeg?auto=compress&cs=tinysrgb&fit=crop&h=1200&w=800",
                "https://images.pexels.com/photos/7365270/pexels-photo-7365270.jpeg?auto=compress&cs=tinysrgb&fit=crop&h=627&w=1200"
            )
        )
        srcs.add(
            Src(
                "https://images.pexels.com/photos/10012826/pexels-photo-10012826.jpeg",
            "https://images.pexels.com/photos/10012826/pexels-photo-10012826.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
        "https://images.pexels.com/photos/10012826/pexels-photo-10012826.jpeg?auto=compress&cs=tinysrgb&h=650&w=940",
        "https://images.pexels.com/photos/10012826/pexels-photo-10012826.jpeg?auto=compress&cs=tinysrgb&h=350",
        "https://images.pexels.com/photos/10012826/pexels-photo-10012826.jpeg?auto=compress&cs=tinysrgb&h=130",
        "https://images.pexels.com/photos/10012826/pexels-photo-10012826.jpeg?auto=compress&cs=tinysrgb&fit=crop&h=1200&w=800",
        "https://images.pexels.com/photos/10012826/pexels-photo-10012826.jpeg?auto=compress&cs=tinysrgb&fit=crop&h=627&w=1200",
        "https://images.pexels.com/photos/10012826/pexels-photo-10012826.jpeg?auto=compress&cs=tinysrgb&dpr=1&fit=crop&h=200&w=280"
            )
        )
        srcs.add(
            Src(
                "https://images.pexels.com/photos/10013212/pexels-photo-10013212.jpeg",
            "https://images.pexels.com/photos/10013212/pexels-photo-10013212.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
        "https://images.pexels.com/photos/10013212/pexels-photo-10013212.jpeg?auto=compress&cs=tinysrgb&h=650&w=940",
        "https://images.pexels.com/photos/10013212/pexels-photo-10013212.jpeg?auto=compress&cs=tinysrgb&h=350",
        "https://images.pexels.com/photos/10013212/pexels-photo-10013212.jpeg?auto=compress&cs=tinysrgb&h=130",
        "https://images.pexels.com/photos/10013212/pexels-photo-10013212.jpeg?auto=compress&cs=tinysrgb&fit=crop&h=1200&w=800",
        "https://images.pexels.com/photos/10013212/pexels-photo-10013212.jpeg?auto=compress&cs=tinysrgb&fit=crop&h=627&w=1200",
        "https://images.pexels.com/photos/10013212/pexels-photo-10013212.jpeg?auto=compress&cs=tinysrgb&dpr=1&fit=crop&h=200&w=280"
            )
        )
        srcs.add(
            Src(
                "https://images.pexels.com/photos/10012544/pexels-photo-10012544.jpeg",
            "https://images.pexels.com/photos/10012544/pexels-photo-10012544.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
        "https://images.pexels.com/photos/10012544/pexels-photo-10012544.jpeg?auto=compress&cs=tinysrgb&h=650&w=940",
        "https://images.pexels.com/photos/10012544/pexels-photo-10012544.jpeg?auto=compress&cs=tinysrgb&h=350",
        "https://images.pexels.com/photos/10012544/pexels-photo-10012544.jpeg?auto=compress&cs=tinysrgb&h=130",
        "https://images.pexels.com/photos/10012544/pexels-photo-10012544.jpeg?auto=compress&cs=tinysrgb&fit=crop&h=1200&w=800",
        "https://images.pexels.com/photos/10012544/pexels-photo-10012544.jpeg?auto=compress&cs=tinysrgb&fit=crop&h=627&w=1200",
        "https://images.pexels.com/photos/10012544/pexels-photo-10012544.jpeg?auto=compress&cs=tinysrgb&dpr=1&fit=crop&h=200&w=280"
            )
        )
        srcs.add(
            Src(
                "https://images.pexels.com/photos/10012571/pexels-photo-10012571.jpeg",
            "https://images.pexels.com/photos/10012571/pexels-photo-10012571.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
        "https://images.pexels.com/photos/10012571/pexels-photo-10012571.jpeg?auto=compress&cs=tinysrgb&h=650&w=940",
        "https://images.pexels.com/photos/10012571/pexels-photo-10012571.jpeg?auto=compress&cs=tinysrgb&h=350",
        "https://images.pexels.com/photos/10012571/pexels-photo-10012571.jpeg?auto=compress&cs=tinysrgb&h=130",
        "https://images.pexels.com/photos/10012571/pexels-photo-10012571.jpeg?auto=compress&cs=tinysrgb&fit=crop&h=1200&w=800",
        "https://images.pexels.com/photos/10012571/pexels-photo-10012571.jpeg?auto=compress&cs=tinysrgb&fit=crop&h=627&w=1200",
        "https://images.pexels.com/photos/10012571/pexels-photo-10012571.jpeg?auto=compress&cs=tinysrgb&dpr=1&fit=crop&h=200&w=280"
            )
        )
        srcs.add(
            Src(
                "https://images.pexels.com/photos/10012572/pexels-photo-10012572.jpeg",
            "https://images.pexels.com/photos/10012572/pexels-photo-10012572.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
        "https://images.pexels.com/photos/10012572/pexels-photo-10012572.jpeg?auto=compress&cs=tinysrgb&h=650&w=940",
        "https://images.pexels.com/photos/10012572/pexels-photo-10012572.jpeg?auto=compress&cs=tinysrgb&h=350",
        "https://images.pexels.com/photos/10012572/pexels-photo-10012572.jpeg?auto=compress&cs=tinysrgb&h=130",
        "https://images.pexels.com/photos/10012572/pexels-photo-10012572.jpeg?auto=compress&cs=tinysrgb&fit=crop&h=1200&w=800",
        "https://images.pexels.com/photos/10012572/pexels-photo-10012572.jpeg?auto=compress&cs=tinysrgb&fit=crop&h=627&w=1200",
        "https://images.pexels.com/photos/10012572/pexels-photo-10012572.jpeg?auto=compress&cs=tinysrgb&dpr=1&fit=crop&h=200&w=280"
            )
        )
        srcs.add(
            Src(
                "https://images.pexels.com/photos/10012573/pexels-photo-10012573.jpeg",
            "https://images.pexels.com/photos/10012573/pexels-photo-10012573.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
        "https://images.pexels.com/photos/10012573/pexels-photo-10012573.jpeg?auto=compress&cs=tinysrgb&h=650&w=940",
        "https://images.pexels.com/photos/10012573/pexels-photo-10012573.jpeg?auto=compress&cs=tinysrgb&h=350",
        "https://images.pexels.com/photos/10012573/pexels-photo-10012573.jpeg?auto=compress&cs=tinysrgb&h=130",
        "https://images.pexels.com/photos/10012573/pexels-photo-10012573.jpeg?auto=compress&cs=tinysrgb&fit=crop&h=1200&w=800",
        "https://images.pexels.com/photos/10012573/pexels-photo-10012573.jpeg?auto=compress&cs=tinysrgb&fit=crop&h=627&w=1200",
        "https://images.pexels.com/photos/10012573/pexels-photo-10012573.jpeg?auto=compress&cs=tinysrgb&dpr=1&fit=crop&h=200&w=280"
            )
        )
        srcs.add(
            Src(
                "https://images.pexels.com/photos/10012402/pexels-photo-10012402.jpeg",
            "https://images.pexels.com/photos/10012402/pexels-photo-10012402.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",
        "https://images.pexels.com/photos/10012402/pexels-photo-10012402.jpeg?auto=compress&cs=tinysrgb&h=650&w=940",
        "https://images.pexels.com/photos/10012402/pexels-photo-10012402.jpeg?auto=compress&cs=tinysrgb&h=350",
        "https://images.pexels.com/photos/10012402/pexels-photo-10012402.jpeg?auto=compress&cs=tinysrgb&h=130",
        "https://images.pexels.com/photos/10012402/pexels-photo-10012402.jpeg?auto=compress&cs=tinysrgb&fit=crop&h=1200&w=800",
        "https://images.pexels.com/photos/10012402/pexels-photo-10012402.jpeg?auto=compress&cs=tinysrgb&fit=crop&h=627&w=1200",
        "https://images.pexels.com/photos/10012402/pexels-photo-10012402.jpeg?auto=compress&cs=tinysrgb&dpr=1&fit=crop&h=200&w=280"
            )
        )

        return srcs
    }

    fun generateDummyDetail(photo: PhotosItem): PhotosItem {
        return PhotosItem(
            photo.src,
            photo.width,
            photo.avgColor,
            photo.photographer,
            photo.photographerUrl,
            photo.id,
            photo.url,
            photo.photographerId,
            photo.liked,
            photo.height)
    }
}
package com.obi.awesomeapp.ui.detail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.obi.awesomeapp.data.PhotosItem
import com.obi.awesomeapp.utils.DataDummy
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule

import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class DetailViewModelTest {

    private lateinit var detailViewModel: DetailViewModel
    private val dummyPhoto = DataDummy.generateDummyPhotos()[0]
    private val photoId = dummyPhoto.id

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var observer: Observer<PhotosItem>

    @Before
    fun before() {
        detailViewModel = mock(DetailViewModel::class.java)
    }

    @Test
    fun `setSelectedPhoto should be success`() {
        detailViewModel.getDetailPhoto(photoId)
        val expected = MutableLiveData<PhotosItem>()
        expected.value = DataDummy.generateDummyDetail(dummyPhoto)

        `when`(detailViewModel.photos).thenReturn(expected)

        detailViewModel.photos.observeForever(observer)

        verify(observer).onChanged(expected.value)
        verify(detailViewModel).getDetailPhoto(photoId)

        val expectedValue = expected.value
        val actualValue = detailViewModel.photos.value

        assertEquals(expectedValue, actualValue)
    }
}
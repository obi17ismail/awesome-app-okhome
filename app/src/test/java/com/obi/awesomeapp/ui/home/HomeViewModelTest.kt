package com.obi.awesomeapp.ui.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.obi.awesomeapp.data.PhotosItem
import com.obi.awesomeapp.utils.DataDummy
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule

import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class HomeViewModelTest {

    private lateinit var homeViewModel: HomeViewModel

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Mock
    private lateinit var observer: Observer<ArrayList<PhotosItem>>

    @Before
    fun before() {
        homeViewModel = mock(HomeViewModel::class.java)
    }

    @Test
    fun `getPhotos should be successs`() {
        val photos = DataDummy.generateDummyPhotos()
        val expected = MutableLiveData<ArrayList<PhotosItem>>()
        expected.value = photos

        `when`(homeViewModel.listPhotos).thenReturn(expected)

        homeViewModel.listPhotos.observeForever(observer)
        verify(observer).onChanged(expected.value)
        verify(homeViewModel).listPhotos

        val expectedValue = expected.value
        val actualValue = homeViewModel.listPhotos.value
        assertEquals(expectedValue, actualValue)
        assertEquals(expectedValue?.size, actualValue?.size)
    }

    @Test
    fun `getPhotos should be success but data is empty`() {
        val photos = DataDummy.generateDummyPhotosEmpty()
        val expected = MutableLiveData<ArrayList<PhotosItem>>()
        expected.value = photos

        `when`(homeViewModel.listPhotos).thenReturn(expected)

        homeViewModel.listPhotos.observeForever(observer)
        verify(observer).onChanged(expected.value)
        verify(homeViewModel).listPhotos

        val actualValueDataSize = homeViewModel.listPhotos.value?.size
        assertTrue("size of data should be 0, actual is $actualValueDataSize", actualValueDataSize == 0)
    }
}
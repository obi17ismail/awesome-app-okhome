package com.obi.awesomeapp.ui.home

import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import com.obi.awesomeapp.R
import com.obi.awesomeapp.utils.DataDummy
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class HomeActivityTest{

    private val dummyPhotos = DataDummy.generateDummyPhotos()

    @Before
    fun setup(){
        ActivityScenario.launch(HomeActivity::class.java)
    }

    @Test
    fun loadPhotos() {
        delayTwoSecond()
        onView(withId(R.id.rvPhotos))
            .check(matches(isDisplayed()))
        onView(withId(R.id.rvPhotos)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(dummyPhotos.size))
        onView(withId(R.id.action_list)).perform(click())
        onView(withId(R.id.rvPhotos)).perform(RecyclerViewActions.scrollToPosition<RecyclerView.ViewHolder>(dummyPhotos.size))
        delayTwoSecond()
        onView(withId(R.id.action_grid)).perform(click())
        delayTwoSecond()
    }

    @Test
    fun loadDetailPhoto() {
        delayTwoSecond()
        onView(withId(R.id.rvPhotos)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0,
                click()
            ))
        delayTwoSecond()
        onView(withId(R.id.tvPhotographerDetail))
            .check(matches(isDisplayed()))
        onView(withId(R.id.tvPhotographerDetail))
            .check(matches(withText(dummyPhotos[0].photographer)))
        onView(withId(R.id.tvUrlDetail))
            .check(matches(isDisplayed()))
        onView(withId(R.id.tvUrlDetail))
            .check(matches(withText(dummyPhotos[0].url)))
        onView(withId(R.id.tvColorDetail))
            .check(matches(isDisplayed()))
        onView(withId(R.id.tvColorDetail))
            .check(matches(withText(dummyPhotos[0].avgColor)))
        onView(withId(R.id.tvLikedDetail))
            .check(matches(isDisplayed()))
        onView(withId(R.id.tvLikedDetail))
            .check(matches(withText(dummyPhotos[0].liked.toString())))
        onView(isRoot()).perform(ViewActions.pressBack())
    }

    private fun delayTwoSecond() {
        try {
            Thread.sleep(2000)
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }
    }
}